# Script Code Libraries in Pure GitLab CI YAML

Using only GitLab CI YAML a standard set of managed script code libraries can be easily used in all jobs throughout the a pipeline with minimal clutter. This is a way of using GitLab CI YAML to package your dependent script code libraries into your pipeline code.

While the examples are in Bash and PowerShell the concepts are transportable to any GitLab non-default shell for which you have a shell installed by combining them with something like this: [Call CMD From Default PowerShell in GitLab CI](https://gitlab.com/guided-explorations/microsoft/windows/call-cmd-from-powershell).

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/cfg-data/write-ci-cd-variables-in-pipeline)

## Demonstrates These Design Requirements, Desirements and AntiPatterns

* **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Uses built-in, rather than custom, stages, including ".pre"
* **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Uses GitLab CI Includes to keep main code neat and clean.

### Script Code Scenarios Covered
- Bash
- PowerShell Core on Linux
- Windows PowerShell on Windows

**NOTE:** Any script code language can be used, such as python, provided that the language processor is installed and available on the containers where you intend to use the script code libraries you encapsulate with this method.

### Uses and Features for This Example:
- Avoid building containers just to transit script code libraries (But Why? => see "Why Avoid Creating Containers with Build Dependencies in Them?" below).
- Seperate concerns of "encapsulating script code libraries dependencies" from containers - thereby freeing you to use containers for managing other per-job runtime requirements. This may reduce the number of custom build containers you maintain even more.
- Clean Coding: Keep script code library definition at the end of the .gitlab-ci.yml or in a completely different include so it does not clutter daily editing.
- Sophisticated Libraries: Predictable libary code loading order (alpha sort on filename) so that purposeful function overrides can be accomplished.
- Handles multiple library files built by as many jobs and/or includes as desired.
- Handles multiple script code languages in the same pipeline by using file name endings to delineate which libraries belong to which languages.
- the pipeline libraries are available to every job in the same pipeline via artifacts passing

### Architectural Heuristics for Why to Avoid Creating Containers / Language Specific Packages with for Bundling Build Dependencies?

**IMPORTANT:** This list of rationale is not intended to try to justify "one method for all time" - rather it is provided precisely so individuals can decide when the method is or **is not** the right approach for the specific implementation under consideration.

While this list is primarily concerned with container based encapsulation of build dependencies (a common GitLab pattern) - it likely also applies to other externalized encapsulation methods such as language package managers (e.g. NPM, Maven, NuGet)

- The pipeline code files function as a single source of truth (SSOT) for all the pipeline code.
- External build dependencies for library code - on container registries and authentication to push to them.
- External runtime dependencies for library code - on container registry (and possibly authentication if not a public registry).
- Stacked release cycles to accomplish builds when CI updates are needed => code, test and build CI container before changing CI for a given job.
- When using shell runners, containers are not available and build code should not be statically stored on build agents.
- You are not building your software into containers the learning curve for using them just for build may not be worth the advantages or you don't have the time for this learning.
- Isolated version safe (tested) copies of libary code are "packaged" along with the specific version of the .gitlab-ci.yml without requiring pegging a container to have the .gitlab-ci.yml match the code in a container.

### Limitations
- GitLab.com and self managed instances limit the maximum size of any included gitlab-ci.yml to approximately 440KB and a maximum of 100 includes.  On a self managed instance, you can set the feature flag `Feature.disable(:ci_yaml_limit_size)` to remove the size limit - but keep in mind it was introduced as a security patch. [Check issue 30098 for more information](https://gitlab.com/gitlab-org/gitlab/-/issues/30098).

### Using This Example
- If your pipeline only requires one script execution type, you can can simply grab only the jobs that start with that script execution type:
  - "bash_" for Linux bash.
  - "linuxpwsh_" for PowerShell Core on Linux.
  - "winpowershell_" for Windows PowerShell.
- If only testing bash, you can avoid waiting for Windows VMs to spin and process by setting DISABLE_WINDOWS_SCENARIOS to a non-null value

## Cross References and Documentation
- [Cloudformation Deploy](https://gitlab.com/guided-explorations/aws/cloudformation-deploy) - working example that uses this method.

## Two Subtypes

### Primary Method Implementation Heuristics: Write Libraries as Files and Transit To All Containers Using Artifacts
- Positive: Libraries can be in includable GitLab CI fragments 
  - Positive: which means it can be used across projects 
  - Positive: and can be separated from main code at the file level
- Positive: all code can be out of the way at the bottom of the file (single or multiple gitlab-ci yml files)
- Positive: multliple script code types can use libraries in the same .gitlab-ci.yml (including resolution of all includes)

### Method 2 Implementation Heuristics: Use YAML Anchors to Store Library Functions
Additional considerations and limitations are in comments in the code for these scenarios. All jobs and files start with "yamlanchor...". ([yamlanchorbashcodelibs.gitlab-ci.yml](./yamlanchorbashcodelibs.gitlab-ci.yml),[yamlanchorpowershellcodelibs.gitlab-ci.yml](./yamlanchorpowershellcodelibs.gitlab-ci.yml))
- Positive: Library code not transited (bandwith & time) or stored on GitLab instance as artifacts
- Positive: Code editor linting may be able to more easily do linting than the heredoc/herestring approach
- Negative: This method can only be used with script languages that are the default GitLab Runner shell on the runner
  - Negative: This rules out other languages, like Python, that can be accommodated by the Primary Method.
- Negative: Libraries must be in the same gitlab-ci yml they are used in (GitLab renders anchors on a per-file, not cross-file basis)
- Negative: Yaml Anchor definitions must appear sequentially before they are used.
  - Negative: This means the only way to locate library code at the bottom of your code (out of the way) is to use a global "before_script" statement
  - Negative: global before_script content must be applicable to every container in a given CI file.
  - Workaround: A per-job yaml anchor method is also shown - but the code resides at the top

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2020-12-18

* **GitLab Version Released On**: 13.6

* **GitLab Edition Required**: 

  * For overall solution: [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

* **References and Featured In**:
  * Walkthrough Video: [Shell Code Libraries in Pure GitLab CI YAML](https://youtu.be/sF3kPJTy2UU)
